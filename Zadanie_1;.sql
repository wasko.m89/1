CREATE DATABASE Baza_1;

USE Baza_1;

CREATE TABLE Biblioteczka(
id INTEGER,
Tytuł TEXT,
Data_zakupu DATE
);

INSERT INTO Biblioteczka (id, Tytuł, Data_zakupu)
VALUES (1,"Sto lat samotności",'20100511');

SELECT * FROM Biblioteczka;

INSERT INTO Biblioteczka (id, Tytuł, Data_zakupu)
VALUES (2, "Bycie i czas",'20150828');

INSERT INTO Biblioteczka (id, Tytuł, Data_zakupu)
VALUES (3,"Idiota",'20180718');

UPDATE Biblioteczka
SET Tytuł = "Bajki"
WHERE id = 1;

ALTER TABLE Biblioteczka ADD COLUMN Autor text;

UPDATE Biblioteczka
SET Autor = "Cyprian Brzozowski"
WHERE id = 1;

UPDATE Biblioteczka
SET Autor = "Martin Heidegger"
WHERE id = 2;

UPDATE Biblioteczka
SET Autor = "Fiodor Dostojewski"
WHERE id = 3;

DELETE FROM Biblioteczka
WHERE id>1;

SELECT Autor FROM Biblioteczka